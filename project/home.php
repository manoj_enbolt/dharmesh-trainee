<!DOCTYPE html>
<html>
<head>
	<title>Home</title>
	<meta charset="utf-8">
 	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" href="owl/dist/assets/owl.carousel.css">
	<link rel="stylesheet" href="owl/dist/assets/owl.theme.default.css">
</head>
<body>
	<nav class="navbar navbar-inverse head">
	  <div class="container-fluid">
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>                        
	      </button>
	    </div>
	    <div class="collapse navbar-collapse" id="myNavbar">
	      <ul class="nav navbar-nav navbar-right">
	        <li><a href="home.php">Home</a></li>
	        <li><a href="about.php">About</a></li>
	        <li><a href="">Contact</a></li>
	        <li><a href="login.php">Login</a></li>
	      </ul>
	    </div>
	  </div>
	</nav>
	<div class="container-fluid">
		<div class="">
			<div class="slider">
				<div id="owl-demo" class="owl-carousel owl-theme">
				  <div class="item"><img src="image/abc.jpg" alt="image"></div>
				  <div class="item"><img src="image/lap1.png" alt="image"></div>
				  <div class="item"><img src="image/slider-kabar.jpg" alt="image"></div>
				</div>
			</div>
		</div>
		<div class="">
			<div class="col-md-12 layout">
				<div class="col-md-4">
					<div class="well">
						<img src="image/lap2.jpg" class="img-circle" height="200" width="200">
						<h1>Apple LapTop</h1>
						<h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
						proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</h4>
					</div>
				</div>
				<div class="col-md-4">
					<div class="well">
						<img src="image/lap3.jpg" class="img-circle" height="200" width="200">
						<h1>Apple LapTop</h1>
						<h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
						proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</h4>
					</div>
				</div>
				<div class="col-md-4">
					<div class="well">
						<img src="image/lap4.jpg" class="img-circle" height="200" width="200">
						<h1>Apple LapTop</h1>
						<h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
						proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</h4>
					</div>
				</div>
			</div>
		</div>
		<div class="">
			<div class="col-md-12 text-center">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d229.51836488633907!2d72.52263884874053!3d23.0129809677706!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x395e84d577f3c7d1%3A0x79d3f65355d3aa4!2sTitanium+City+Center+Mall!5e0!3m2!1sen!2sin!4v1532543441257"  frameborder="0" style="border:0;" allowfullscreen height="800" width="1000"></iframe>
			</div>
		</div> 
	</div>
	<footer class="container-fluid text-center">
		<p class="p_center"><h4><span style="color:red">Created by</span> <a href="">DHARMESH SONAGRA</a></h4></p>
	</footer>	

  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="owl/dist/owl.carousel.js"></script>
	<script>
		$(document).ready(function() {
	  		$("#owl-demo").owlCarousel({
 			loop:true,
	  		autoplay: true,
		    navigation : true, // Show next and prev buttons
		    slideSpeed : 100,
		    dots:true,
		    items : 1, 
		    //paginationSpeed : 400,
		    // singleItem:true,
			// "singleItem:true" is a shortcut for:
		    // itemsDesktop : false,
		    // itemsDesktopSmall : false,
		    // itemsTablet: false,
		    // itemsMobile : false,
		 
		  });
		});
	</script>
</body>
</html>
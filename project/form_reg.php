<!DOCTYPE html>
<html>
<head>
  <title>Login</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>
        body {font-family: Arial, Helvetica, sans-serif;
              background-color: rgb(10,100,100);}
        input[type=text], input[type=password] {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            box-sizing: border-box;
        }
        .imgcontainer {
            text-align: center;
            margin: 24px 0 12px 0;
        }

        img.avatar {
            width: 20%;
            border-radius: 20%;
        }

        .container {
            padding: 20px;
            width: 500px;
        }
        h2{
            color: rgb(20,200,20);
        }

</style>
</head>
<body>
      <form action="reg.php" method="post">
        <div class="imgcontainer">
          <img src="image/img_avatar2.png" alt="Avatar" class="avatar">
          <h2>Register Form</h2>
        </div>

        <div class="container">
          <label for="uname"><b>UserEmail</b></label>
          <input type="text" placeholder="Enter UserEmail" name="email" required>

          <label for="psw"><b>Password</b></label>
          <input type="password" placeholder="Enter Password" name="password" required>
          
          <input class='btn-success'type="submit" name="submit" value="Register Now">              
        </div>
      </form>
</body>
</html>
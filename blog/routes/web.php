<?php
Auth::routes();

Route::get('/', 'HomeController@index');
Route::get('post/{slug}', 'HomeController@showPage');
Route::get('category/{category}', 'HomeController@showCategory');
Route::get('search', 'HomeController@search');

Route::group(['prefix' => 'admin'], function(){
	Route::get('login', 'AuthAdminController@loginShowForm');
	Route::post('login', 'AuthAdminController@login')->name('login');
	Route::get('dashboard', 'AdminController@dashboard');
	Route::post('logout', 'AdminController@logout')->name('admin.logout');
	Route::resource('post','AdminPostController');
	Route::resource('category','AdminCategoryController');
	Route::resource('tag','AdminTagController');
	Route::post('search', 'AdminController@search');
});
	
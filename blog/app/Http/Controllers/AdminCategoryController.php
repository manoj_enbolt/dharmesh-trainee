<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Admin;
use Auth;
use App\Category;

class AdminCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index()
    {
    	$category = Category::paginate(5);
    	return view('admin.category.index')->with(['category' => $category]);
    }
    public function create()
    {
    	return view('admin.category.create');
    }
    public function show()
    {
    	//
    }
    public function store(Request $request)
    {

    	//dd('asas');
    	$this->validate($request, [
    		'name' => 'required',
    		'description' => 'required',
        ]);
        $category = new Category();
        $category->name = $request->name;
        $category->description = $request->description;
        $category->save();
        return redirect('admin/category');
    }
    public function edit($id)
    {
    	$category = Category::find($id);
    	return view('admin.category.edit',compact('category', 'id'))
					->with(['category' => $category]);
    }
    public function update(Request $request, $id)
    {
    	$this->validate($request, [
    		'name' => 'required',
    		'description' => 'required',
        ]);
        $category = Category::find($id);
        $category->name = $request->get('name');
        $category->description = $request->get('description');
        $category->update();
        return redirect('admin/category');
    }
    public function destroy($id)
    {
    	$category = Category::find($id);
    	$category->delete();
    	return redirect('admin/category');
    }
}

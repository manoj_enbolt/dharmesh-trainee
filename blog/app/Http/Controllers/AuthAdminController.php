<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Admin;
use Auth;

class AuthAdminController extends Controller
{
	/**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // /
    }

     public function loginShowForm()
    {
    	if (Auth::guard('admin')->check()) {
		    return redirect('admin/dashboard');
		} else {
	    	return view('admin.login');
		}
    }
    public function login(Request $request)
    {
        $this->validate($request, [
            'email'     => 'required|email',
            'password'  => 'required|min:6'
        ]);
        if (Auth::guard('admin')->attempt([
        	'email' => $request->email,
        	'password' => $request->password
     		], 
     		$request->remember) ) {
            	return redirect('admin/dashboard');
        	}
            return redirect()->back()->with('error', 'Credential not match');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Admin;
use Auth;
use App\Tag;

class AdminTagController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
     public function index()
    {
    	//dd('sss');
    	$tag = Tag::all();
    	return view('admin.tags.index')->with(['tag' => $tag]);
    }
    public function show()
    {

    }
    public function create()
    {
    	return view('admin.tags.create');
    }
    public function store(Request $request)
    {
    	//dd('asas');
    	$this->validate($request, [
    		'name' => 'required',
    		
        ]);
        $tag = new Tag();
        $tag->name = $request->name;
        $tag->save();
        return redirect('admin/tag');
    }
    public function edit($id)
    {
        $tag = Tag::find($id);
        return view('admin.tags.edit', compact('tag', 'id'))
                    ->with(['tag' => $tag]);
    }
    public function update(Request $request, $id)
    {
        $this->validate($request, [
    		'name' => 'required',
        ]);
        //file update
        $tag = Tag::find($id);
        $tag->name = $request->get('name');
        $tag->update();
        return redirect('admin/tag');
    }
    public function destroy($id)
    {
        $tag = Tag::find($id);
        $tag->delete();
        return redirect('admin/tag');
    }
}

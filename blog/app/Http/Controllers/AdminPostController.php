<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Admin;
use Auth;
use App\Post;
use App\Category;
use App\Tag;

class AdminPostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index()
    {
        $post = Post::paginate(5);
    	return view('admin.post.index')->with(['post' => $post]);
    }
    public function show()
    {

    }
    public function create()
    {
        $categories = Category::select(['id', 'name'])->get()->toArray();
        foreach ($categories as $value) {
            $data[$value['id']] =  $value['name'];
        }
        $categories = Tag::select(['id', 'name'])->get()->toArray();
        foreach ($categories as $value) {
            $tag[$value['id']] =  $value['name'];
        }
    	return view('admin.post.create')->with(['data' => $data, 'tag' => $tag]);
    }
    public function store(Request $request)
    {
    	//dd('asas');
    	$this->validate($request, [
            /*'user_id' => 'required|numeric',*/
    		'title' => 'required',
    		'content' => 'required',
    		'file' => 'required',
    		/*'view_id' => 'required|numeric',*/
        ]);
        $filename = $request->file->getClientOriginalName();
        $request->file->storeAs('public/upload',$filename);
        
       
        $post = new Post();
        $post->user_id = Auth::user()->name;
        $post->title = $request->title;
        $post->content = $request->content;
        $post->category_id = $request->category_id;
        $post->slug = textToSlug($request->title);
        $post->tags = implode(',', $request->tags);
        $post->file = $filename;
        $post->save();
        return redirect('admin/post');
    }
    public function edit($id)
    {
        $categories = Category::select(['id', 'name'])->get()->toArray();
        foreach ($categories as $value) {
            $data[$value['id']] =  $value['name'];
        }
        $categories = Tag::select(['id', 'name'])->get()->toArray();
        foreach ($categories as $value) {
            $tag[$value['id']] =  $value['name'];
        }
        $post = Post::find($id);
        return view('admin.post.edit', compact('post', 'id'))
                ->with(['post' => $post,'data' => $data, 'tag' => $tag]);
    }
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            /*'user_id' => 'required|numeric',*/
    		'title' => 'required',
    		'content' => 'required',
    		'file' => 'required',
        ]);
        //file update
        $filename = $request->file->getClientOriginalName();
        $request->file->storeAs('public/upload',$filename);

        $post = Post::find($id);
        /*$post->user_id = $request->get('user_id');*/
        $post->title = $request->get('title');
        $post->content = $request->get('content');
        //$post->view_id = $request->get('view_id');
        $post->category_id = $request->get('category_id');
        $post->tags = implode(',', $request->get('tags'));
        $post->file = $filename;
        $post->update();
        return redirect('admin/post');
    }
    public function destroy($id)
    {
        $post = Post::find($id);
        $post->delete();
        return redirect('admin/post');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Admin;
use Auth;
use App\Post;
use DB;


class AdminController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function dashboard()
    {	
    	return view('admin.dashboard');
    }
    public function logout(Request $request)
    {
    	Auth::guard('admin')->logout();
        $request->session()->flush();
        $request->session()->regenerate();
        return redirect()->guest(route( 'login' ));
    }
    public function search(Request $request)
    {
       
        $articles = DB::table('posts')->where('title', 'LIKE',  $request->search . '%')->paginate(5);
        return view('admin.post.search', compact('articles', 'query'));

    } 
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Category;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        /*$this->middleware('auth');*/
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //dd('ss');
        //$post = Post::paginate(10);
        $posts = DB::table('posts')
            ->select('posts.*', 'categories.name')
            ->join('categories', 'posts.category_id', '=', 'categories.id')
            ->paginate(5);
            
        //dd($posts);

        return view('home')->with(['posts' => $posts]);
    }
    public function showPage($slug)
    {
        $view_count = Post::where('slug', $slug)->increment('view_count', 1);
        $post = Post::where('slug', $slug)->first();
        $category = Category::where('id', $post->category_id)->first();
        return view('showpage')->with(['post' => $post, 'categoty' => $category, 'view_count' => $view_count]);
    }
    public function showCategory($category_id)
    {
        $category_post= Post::where('category_id', $category_id)->paginate(10);
        $category = Category::where('id', $category_id)->first();
        return view('showCategory')->with(['category_post'=> $category_post,'category' => $category]);
    }
    public function search(Request $request)
    {
        $posts = DB::table('posts')
                ->select('posts.*', 'categories.name')
                ->join('categories', 'posts.category_id', '=', 'categories.id')
                ->where('title', 'like',  '%'.$request->search .'%')
                ->paginate(10);
        return view('home')->with(['posts' => $posts]);
    }
    
}

@extends('layouts.admin')

@section('content')
    <div class="box-body">
        <h4>Create Tags</h4>
        {{ Form::open(['url' => 'admin/tag', 'method' => 'post'])}}
            <div class="form-group" {{$errors->has('name') ? 'is-valid' : ''}}>
                <label for="name">Name</label>
                {{ Form::text('name', '', ['placeholder' => 'Name', 'class' => 'form-control']) }}
                {!! $errors->first('name','<small style="color:red;">:message</small>') !!}
            </div>
            <div class="form-group">
                {{ Form::submit('submit', ['class' => 'btn btn-success mr-2']) }}
            </div>  
        {{ form::close()}}
    </div>                               
@endsection

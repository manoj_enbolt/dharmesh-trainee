  @extends('layouts.admin')

  @section('content')
        {{ Form::open(['route' => ['tag.update', $tag->id], 'method' => 'post'])}}
        {{ method_field('PATCH') }}
        <div class="box-body">
            <h4>Edit Tags</h4>
            <div class="form-group" {{ $errors->has('name') ? 'is-invalid' : ''}}>
              <label for="title">name</label>
              {{ Form::text('name', $tag->name, ['placeholder' => 'Title', 'class' => 'form-control']) }}
              {!! $errors->first('name','<small style="color:red;">:message</small>') !!}
            </div>
            <div class="form-group">
              {{ Form::submit('submit', ['class' => 'btn btn-primary']) }}
            </div>  
        {{ form::close()}}           
    </div>      
@endsection

@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h1>Tags</h1>
                    <a role="button" class="btn btn-success" href="tag/create">Create Tags</a>
                    <div style="float: right;">
                        {!! Form::open(['url' => 'admin/search', 'method' => 'post', 'class'=>'form navbar-form navbar-right searchform']) !!}
                            {!! Form::text('search', null,['class'=>'form-control','placeholder'=>'Search for a Post...']) !!}
                            {!! Form::submit('Search',['class'=>'btn btn-default']) !!}
                        {!! Form::close() !!}
                    </div>
                    <div class="table-responsive">
                        <table id="example2" class="table">
                            <thead>
                                <tr>
                                    <th><h4>Id</h4></th>
                                    <th><h4>Name</h4></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($tag as $row)
                                    <tr>
                                        <td>{{$row->id }}</td>
                                        <td>{{$row->name }}</td>
                                        <td>
                                            <a role="button" class="btn btn-primary" href="{{ URL::route('tag.edit', $row->id) }}">Edit</a>

                                                <form id="delete-form-{{ $row->id }}" method="post" action="{{ route('tag.destroy', $row->id) }}" style="display: none;">
                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }}
                                                </form>
                                            <a class="btn btn-danger" href="{{ URL::route('tag.destroy', $row->id) }}" onclick="
                                                if (confirm('Are you really want to Delete This Post?')) {
                                                    event.preventDefault();
                                                    document.getElementById('delete-form-{{ $row->id }}').submit();
                                                } else {
                                                    event.preventDefault();
                                                }">Delete
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>    
                </div>
            </div>
        </div>
    </div>    
@endsection
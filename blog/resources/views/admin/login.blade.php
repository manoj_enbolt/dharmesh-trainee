{{-- @extends('layouts.auth')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Login') }}</div>
                        <div class="card-body">
                            @if(Session::has('error'))   
                                <div class="alert alert-danger">
                                  {{ Session::get('error')}} 
                                </div>
                            @endif
                            <form method="POST" action="{{ url('admin/login') }}">
                                @csrf

                                <div class="form-group row">
                                    <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                    <div class="col-md-6">
                                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}">

                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                    <div class="col-md-6">
                                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password">

                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-6 offset-md-4">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                            <label class="form-check-label" for="remember">
                                                {{ __('Remember Me') }}
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-8 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Login') }}
                                        </button>

                                        <a class="btn btn-link" href="{{ route('password.request') }}">
                                            {{ __('Forgot Your Password?') }}
                                        </a>
                                    </div>
                                </div>
                            </form>
                        </div>    
                    </div>
                </div>
            </div>
        </div>    
@endsection --}}
<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Blog</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="/vendor/backend/iconfonts/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="/vendor/backend/css/vendor.bundle.base.css">
  <link rel="stylesheet" href="/vendor/backend/css/vendor.bundle.addons.css">
  <link rel="stylesheet" href="/style.css">

</head>
<body>
  <!-- <link rel="stylesheet" href="/css/backend_css/stylea.css"> -->
<div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper auth-page">
        <div class="content-wrapper d-flex align-items-center auth auth-bg-1 theme-one">
            <div class="row w-100">
                <div class="col-lg-4 mx-auto">
                    <div class="auto-form-wrapper">
                         @if(Session::has('error'))   
                                <div class="alert alert-danger">
                                  {{ Session::get('error')}} 
                                </div>
                            @endif
                        <form method="POST" action="{{ url('admin/login') }}">
                            @csrf
                            <div class="form-group">
                                <label class="label">Email</label>
                                <div class="input-group" {{ $errors->has('email') ? ' is-invalid' : '' }}>
                                    <input name="email" type="text" class="form-control" placeholder="Email">
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="mdi mdi-check-circle-outline"></i>
                                        </span>
                                    </div>
                                    {!! $errors->first('email','<small style="color:red;">:message</small>') !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="label">Password</label>
                                <div class="input-group" {{ $errors->has('password') ? ' is-invalid' : '' }}>
                                        <input name="password" type="password" class="form-control" placeholder="*********">
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="mdi mdi-check-circle-outline"></i>
                                            </span>
                                        </div>
                                        {!! $errors->first('password','<small style="color:red;">:message</small>') !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <button name="submit" type="submit" class="btn btn-primary submit-btn btn-block">Login</button>
                            </div>
                        </form>
                    </div>
                    <p class="footer-text text-center">copyright © 2018 Bootstrapdash. All rights reserved.</p>
                </div>
            </div>
        </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
</div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <script src="/vendor/backend/js/vendor.bundle.base.js"></script>
  <script src="/vendor/backend/js/vendor.bundle.addons.js"></script>
  <!-- endinject -->
  <!-- inject:js -->
  <script src="/js/backend_js/off-canvas.js"></script>
  <script src="/js/backend_js/misc.js"></script>
  <!-- endinject -->
</body>

</html>

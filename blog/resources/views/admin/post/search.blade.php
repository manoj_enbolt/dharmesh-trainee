@extends('layouts.admin')

@section('content')
	<div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                     <h4 class="card-title">Post</h4>
                    <a role="button" class="btn btn-success" href="post/create" {{-- style="float: right;" --}}>Create Post</a>
					@if (count($articles) === 0)
						<div style="color:red;">Record Not Found</div>
					@elseif (count($articles) >= 1)	
					<div style="float: right;">
			            {!! Form::open(['url' => 'admin/search', 'method' => 'post', 'class'=>'form navbar-form navbar-right searchform']) !!}
			                {!! Form::text('search', null,['class'=>'form-control','placeholder'=>'Search for a Post...']) !!}
			                {!! Form::submit('Search',['class'=>'btn btn-default']) !!}
			            {!! Form::close() !!}
			        </div>
			        <div class="table-responsive" >
						<table id="example2" class="table">
					            <thead>
					                <tr>
					                    <th><h4>Id</h4></th>
					                    <th><h4>Title</h4></th>
					                    <th><h4>Content</h4></th>
					                    <th><h4>Cotegory</h4></th>
					                    <th><h4>Tags</h4></th>
					                    <th><h4>Image</h4></th>
					                    <th><h4>Action</h4></th>
					                </tr>
					            </thead>
					            <tbody>
					                @foreach($articles as $row)
					                    <tr>
					                        <td>{{$row->id }}</td>
					                        <td>{{$row->title }}</td>
					                        <td>{{$row->content }}</td>
					                        <td>{{$row->category_id}}</td>
					                        <td>{{$row->tags}}</td>
					                        <td><img src="{{ asset('storage/upload/'.$row->file) }}" height="100%"  width="100%"></td>
					                        <td>
					                            <a role="button" class="btn btn-primary" href="{{ URL::route('post.edit', $row->id) }}">Edit</a>

					                                <form id="delete-form-{{ $row->id }}" method="post" action="{{ route('post.destroy', $row->id) }}" style="display: none;">
					                                    {{ csrf_field() }}
					                                    {{ method_field('DELETE') }}
					                                </form>
					                            <a class="btn btn-danger" href="{{ URL::route('post.destroy', $row->id) }}" onclick="
					                                if (confirm('Are you really want to Delete This Post?')) {
					                                    event.preventDefault();
					                                    document.getElementById('delete-form-{{ $row->id }}').submit();
					                                } else {
					                                    event.preventDefault();
					                                }">Delete
					                            </a>
					                        </td>
					                    </tr>
					                @endforeach
					            </tbody>
					    </table>
					</div>
	@endif
@endsection
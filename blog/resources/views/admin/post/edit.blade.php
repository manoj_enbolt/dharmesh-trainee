  @extends('layouts.admin')

  @section('content')
        {{ Form::open(['route' => ['post.update', $post->id], 'method' => 'post', 'files' => true])}}
        {{ method_field('PATCH') }}
        <div class="box-body">
            <h4>Edit Post</h4>
            {{-- <div class="form-group" {{$errors->has('user_id') ? 'is-valid' : ''}}>
                <label for="name">User Id</label>
                {{ Form::text('user_id', '', ['placeholder' => 'User Id','class' => 'form-control']) }}
                {!! $errors->first('user_id','<small style="color:red;">:message</small>') !!}
            </div> --}}
            <div class="form-group" {{ $errors->has('title') ? 'is-invalid' : ''}}>
              <label for="title">Title</label>
              {{ Form::text('title', $post->title, ['placeholder' => 'Title', 'class' => 'form-control']) }}
              {!! $errors->first('title','<small style="color:red;">:message</small>') !!}
            </div>
            <div class="form-group" {{ $errors->has('content') ? 'is-invalid' : ''}}>
              <label for="Contenet">Content</label>
              {{ Form::textarea('content', $post->content, ['placeholder' => 'Content', 'class' => 'form-control','id' => 'editor1']) }}
              {!! $errors->first('content','<small style="color:red;">:message</small>') !!}
            </div>
            <div class="form-group">
                <label for="category">Category</label>
                {{ Form::select('category_id', $data,'',['class' => 'form-control']) }}
            </div>
            <div class="form-group">
                <label for="tags">Tags</label>
                {{ Form::select('tags[]', $tag,'', ['multiple' => 'multiple','class' => 'form-control tag']) }}
            </div>
            <div class="form-group" {{ $errors->has('file') ? 'is-invalid' : ''}}>
              <label for="file">File input</label>
              {{ Form::file('file',['class' => 'form-control']) }}
              {!!$errors->first('file','<small style="color:red;">:message</small>')!!} 
            </div>
            <div class="form-group">
              <img src="{{ asset('storage/upload/'.$post->file) }}" height="100px" width="150px">
            </div> 
            <div class="form-group">
              {{ Form::submit('submit', ['class' => 'btn btn-primary']) }}
            </div>  
        {{ form::close()}}           
    </div>      
@endsection

@extends('layouts.admin')

@section('content')
    <div class="box-body">
        <h4>Create Post</h4>
        {{ Form::open(['url' => 'admin/post', 'method' => 'post', 'files' => true])}}
            {{-- <div class="form-group" {{$errors->has('user_id') ? 'is-valid' : ''}}>
                <label for="name">User Id</label>
                {{ Form::text('user_id', '', ['placeholder' => 'User Id','class' => 'form-control']) }}
                {!! $errors->first('user_id','<small style="color:red;">:message</small>') !!}
            </div> --}}
            <div class="form-group" {{$errors->has('title') ? 'is-valid' : ''}}>
                <label for="name">Title</label>
                {{ Form::text('title', '', ['placeholder' => 'Title', 'class' => 'form-control']) }}
                {!! $errors->first('title','<small style="color:red;">:message</small>') !!}
            </div>
             <div class="form-group" {{$errors->has('content') ? 'is-valid' : ''}}>
                <label for="name">Content</label>
                {{ Form::textarea('content', '', ['class' => 'form-control','id' => 'editor1'])}}
                {!! $errors->first('content','<small style="color:red;">:message</small>') !!}
            </div>
            <div class="form-group" {{$errors->has('file') ? 'is-valid' : ''}}>
                <label for="file">File Input</label>
                {{ Form::file('file', ['class' => 'form-control']) }}
                {!! $errors->first('file','<small style="color:red;">:message</small>') !!}
            </div> 
            <div class="form-group">
                <label for="category">Category</label>
                {{ Form::select('category_id', $data, '',['class' => 'form-control'] ) }}
            </div>
            <div class="form-group">
                <label for="tags">Tags</label>
                {{ Form::select('tags[]', $tag,'', ['multiple' => 'multiple','class' => 'form-control tag']) }}
            </div>
            {{-- <div class="form-group" {{$errors->has('view_id') ? 'is-valid' : ''}}>
                <label for="name">View Id</label>
                {{ Form::text('view_id', '', ['placeholder' => 'view Id', 'class' => 'form-control']) }}
                {!! $errors->first('view_id','<small style="color:red;">:message</small>') !!}
            </div> --}}
            <div class="form-group">
                {{ Form::submit('submit', ['class' => 'btn btn-success mr-2']) }}
            </div>  
        {{ form::close()}}
    </div>                               
@endsection

@extends('layouts.admin')

@section('content')
    <div class="box-body">
        <h4>Edit Category</h4>
        {{ Form::open(['route' => ['category.update', $category->id], 'method' => 'post'])}}
        {{ method_field('PATCH') }}
            <div class="form-group" {{$errors->has('name') ? 'is-valid' : ''}}>
                <label for="name">Name</label>
                {{ Form::text('name', $category->name, ['placeholder' => 'Name', 'class' => 'form-control']) }}
                {!!$errors->first('name', '<div style="color:red";>:message</div>') !!}
            </div>
             <div class="form-group" {{$errors->has('description') ? 'is-valid' : ''}}>
                <label for="name">Description</label>
                {{ Form::textarea('description', $category->description, ['class' => 'form-control'])}}
                {!!$errors->first('description', '<div style="color:red";>:message</div>') !!}
            </div>
            <div class="form-group">
                {{ Form::submit('submit', ['class' => 'btn btn-success mr-2']) }}
            </div>  
        {{ form::close()}}
    </div>                               
@endsection
@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4>CATEGORY</h4>
                    <a role="button" class="btn btn-success" href="{{route('category.create')}}">Create Category</a>
                    <div style="float: right;">
                        {!! Form::open(['url' => 'admin/search', 'method' => 'post', 'class'=>'form navbar-form navbar-right searchform']) !!}
                            {!! Form::text('search', null,['class'=>'form-control','placeholder'=>'Search for a Post...']) !!}
                            {!! Form::submit('Search',['class'=>'btn btn-default']) !!}
                        {!! Form::close() !!}
                    </div>
                    <div class="table-responsive">
                        <table id="example2" class="table">
                            <thead>
                                <tr>
                                    <th><h4>Id</h4></th>
                                    <th><h4>Name</h4></th>
                                    <th><h4>Description</h4></th>
                                    <th><h4>Action</h4></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($category as $row)
                                    <tr>
                                        <td>{{$row->id }}</td>
                                        <td>{{$row->name }}</td>
                                        <td>{{$row->description }}</td>
                                        <td>
                                            <a role="button" class="btn btn-primary" href="{{ URL::route('category.edit', $row->id) }}">Edit</a>

                                                <form id="delete-form-{{ $row->id }}" method="post" action="{{ route('category.destroy', $row->id) }}" style="display: none;">
                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }}
                                                </form>
                                            <a class="btn btn-danger" href="{{ URL::route('category.destroy', $row->id) }}" onclick="
                                                if (confirm('Are you really want to Delete This Category?')) {
                                                    event.preventDefault();
                                                    document.getElementById('delete-form-{{ $row->id }}').submit();
                                                } else {
                                                    event.preventDefault();
                                                }">Delete
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $category->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection
@extends('layouts.admin')

@section('content')
    <div class="box-body">
        <h4>Create Category</h4>
        {{ Form::open(['url' => 'admin/category', 'method' => 'post'])}}
            <div class="form-group" {{$errors->has('name') ? 'is-valid' : ''}}>
                <label for="name">Name</label>
                {{ Form::text('name', '', ['placeholder' => 'Name', 'class' => 'form-control']) }}
                {{-- {{ $errors->first('name', '<div style="colore:red;">:message</div>')}} --}}
                {!! $errors->first('name','<small style="color:red;">:message</small>') !!}
            </div>
             <div class="form-group" {{$errors->has('description') ? 'is-valid' : ''}}>
                <label for="name">Description</label>
                {{ Form::textarea('description', '', ['class' => 'form-control'])}}
                {!! $errors->first('description','<small style="color:red;">:message</small>') !!}
            </div>
            <div class="form-group">
                {{ Form::submit('submit', ['class' => 'btn btn-success mr-2']) }}
            </div>  
        {{ form::close()}}
    </div>                               
@endsection

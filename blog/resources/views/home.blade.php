@extends('layouts.frontend')
    @section('content')
        <!-- Page Header -->
        <header class="masthead" style="background-image: url('image/home-bg.jpg')">
          <div class="overlay"></div>
          <div class="container">
            <div class="row">
              <div class="col-lg-8 col-md-10 mx-auto">
                <div class="site-heading">
                  <h1> Blog</h1>
                  <span class="subheading">A Blog Theme by Dharmesh</span>
                </div>
              </div>
            </div>
          </div>
        </header>
        <!-- Main Content -->
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-10 mx-auto">
                    @foreach($posts as $row)
                        <div class="post-preview">
                            <a href="{{url('post/'.$row->slug)}}">
                                <h2 class="post-title">
                                    {{$row->title}}
                                </h2>
                            </a>
                            <p class="post-meta">Posted by
                                {{$row->name}} CreateAt {!! date('d-m-Y',strtotime($row->created_at)) !!} Views {{$row->view_count}}
                            </p>
                              
                        </div>
                        <div>
                            <p>{!! html_entity_decode(substr($row->content,0,200)) !!}...</p>
                        </div>
                        <hr>
                    @endforeach
                    {{ $posts->links() }}
                </div>
                @include('layouts.sidebar')
            </div>
        </div>
    @endsection    

        
        


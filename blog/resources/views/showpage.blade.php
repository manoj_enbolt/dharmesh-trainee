@extends('layouts.frontend')
  @section('content')
    <!-- Page Header -->
    <header class="masthead" style="background-image:url({{asset('storage/upload/'.$post->file)}});">
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto">
            <div class="post-heading">
              <h1>{{ $post->title }}</h1>
              <span class="meta">Posted by 
                <a href="#">{{ $post->user_id }}</a>
                on {!! date('d-m-Y',strtotime($post->created_at)) !!}</span>
            </div>
          </div>
        </div>
      </div>
    </header>

    <!-- Post Content -->
    <article>
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto">
              <div class="post-preview">
                {{-- <p>{!! html_entity_decode($row->content) !!}</p> --}}
                Category:<a role="button" class="btn btn-success" href="{{ url('category/'.$post->category_id) }}"> {{ $categoty->name}}</a>
                {!! html_entity_decode($post->content) !!}
              </div>
              <hr>
              <div id="disqus_thread"></div>
              <script>
              (function() { // DON'T EDIT BELOW THIS LINE
              var d = document, s = d.createElement('script');
              s.src = 'https://project-jtfqqckmaf.disqus.com/embed.js';
              s.setAttribute('data-timestamp', +new Date());
              (d.head || d.body).appendChild(s);
              })();
              </script>
              <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
          </div>
           @include('layouts.sidebar')
        </div>
      </div>
    </article>
@endsection

   
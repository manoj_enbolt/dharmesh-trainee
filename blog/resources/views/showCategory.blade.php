@extends('layouts.frontend')
    @section('content')
        <!-- Page Header -->
        <header class="masthead" style="background-image: url('image/home-bg.jpg')">
          <div class="overlay"></div>
          <div class="container">
            <div class="row">
              <div class="col-lg-8 col-md-10 mx-auto">
                <div class="site-heading">
                  <h1>{{ $category->name }}</h1>
                  <span class="subheading">Description:{{ $category->description}}</span>
                </div>
              </div>
            </div>
          </div>
        </header>
        <!-- Main Content -->
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-10 mx-auto">
                    @foreach($category_post as $row)
                        <div class="post-preview">
                            <a href="{{url('post/'.$row->slug)}}">
                                <h2 class="post-title">
                                    {{$row->title }}
                                </h2>
                            </a>
                            <p class="post-meta">Posted by
                                {{$row->name}} CreateAt {!! date('d-m-Y',strtotime($row->created_at)) !!} {{-- Views {{$row->view_id}} --}}
                            </p>
                              
                        </div>
                        <div>
                            <p>{!! substr($row->content,0,200) !!}...</p>
                        </div>
                        <hr>
                    @endforeach
                    {{ $category_post->links() }}
                </div> 
                 @include('layouts.sidebar')
            </div>
        </div>
    @endsection
        

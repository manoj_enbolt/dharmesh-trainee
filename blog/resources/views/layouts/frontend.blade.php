<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Blog</title>

        <!-- Bootstrap core CSS -->
        <link href="{{asset('vendor/frontend/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

        <!-- Custom fonts for this template -->
        <link href="{{asset('vendor/frontend/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
        <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

        <!-- Custom styles for this template -->
        <link href="{{asset('css/frontend_css/clean-blog.min.css')}}" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{asset('css/frontend_css/prism.css')}}">
    </head>

    <body>
        <!-- Navigation -->
        <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
          <div class="container">
            <a class="navbar-brand" href="">Start Blog</a>
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
              Menu
              <i class="fas fa-bars"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
              <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                  <a class="nav-link" href="{{ url('/') }}">Home</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="">About</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="">Sample Post</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="">Contact</a>
                </li>
                <li class="nav-item">
                    {!! Form::open(['url' => 'search', 'method' => 'get', 'class'=>'form navbar-form navbar-right searchform']) !!}
                           {{--  {!! Form::text('search', null,['class'=>'form-control','placeholder'=>'Search...']) !!}
                            {!! Form::submit('Search',['class'=>'btn btn-default']) !!} --}}
                        <div class="input-group mb-3">
                          <input name="search" type="text" class="form-control" placeholder="Search" aria-label="Recipient's username" aria-describedby="button-addon2">
                          <div class="input-group-append">
                            <button class="btn btn-outline-secondary" type="submit" id="button-addon2">Search</button>
                          </div>
                        </div>
                    {!! Form::close() !!}

                </li>
              </ul>
            </div>
          </div>
        </nav>
        @yield('content')
        <hr>
        <!-- Footer -->
        <footer>
          <div class="container">
            <div class="row">
              <div class="col-lg-8 col-md-10 mx-auto">
                <ul class="list-inline text-center">
                  <li class="list-inline-item">
                    <a href="#">
                      <span class="fa-stack fa-lg">
                        <i class="fas fa-circle fa-stack-2x"></i>
                        <i class="fab fa-twitter fa-stack-1x fa-inverse"></i>
                      </span>
                    </a>
                  </li>
                  <li class="list-inline-item">
                    <a href="#">
                      <span class="fa-stack fa-lg">
                        <i class="fas fa-circle fa-stack-2x"></i>
                        <i class="fab fa-facebook-f fa-stack-1x fa-inverse"></i>
                      </span>
                    </a>
                  </li>
                  <li class="list-inline-item">
                    <a href="#">
                      <span class="fa-stack fa-lg">
                        <i class="fas fa-circle fa-stack-2x"></i>
                        <i class="fab fa-github fa-stack-1x fa-inverse"></i>
                      </span>
                    </a>
                  </li>
                </ul>
                <p class="copyright text-muted">Copyright &copy; Your Website 2018</p>
              </div>
            </div>
          </div>
        </footer>
        <!-- Bootstrap core JavaScript -->
        <script src="{{asset('vendor/frontend/jquery/jquery.min.js')}}"></script>
        <script src="{{asset('vendor/frontend/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
        <!-- Custom scripts for this template -->
        <script src="{{asset('js/frontend_js/clean-blog.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/frontend_js/prism.js')}}"></script>

    </body>
</html>


<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Blog</title>
        <!-- plugins:css -->
        {{-- <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" /> --}}
{{--         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
 --}}        <link rel="stylesheet" href="{{ asset('vendor/backend/iconfonts/mdi/css/materialdesignicons.min.css')}}">
        <link rel="stylesheet" href="{{ asset('vendor/backend/css/vendor.bundle.base.css')}}">
        <link rel="stylesheet" href="{{ asset('vendor/backend/css/vendor.bundle.addons.css')}}">
        {{-- <link rel="stylesheet" href="{{ asset('vendor/css/style.css')}}"> --}}
        <link rel="stylesheet" href="{{ asset('css/backend_css/style.css')}}">
        <!-- endinject -->
    </head>
    <body>
        <div class="container-scroller">
            <!-- partial:partials/_navbar.html -->
            <nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
                <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
                    <a class="navbar-brand brand-logo" href="">
                        <img src="{{asset('image/logo.svg')}}" alt="logo" />
                    </a>
                    <a class="navbar-brand brand-logo-mini" href="index.html">
                        <img class='img-xs rounded-circle' src="{{asset('image/d.png')}}" alt="logo" />
                    </a>
                </div>
                <div class="navbar-menu-wrapper d-flex align-items-center">
                    <ul class="navbar-nav navbar-nav-right">
                        <li class="nav-item dropdown d-none d-xl-inline-block">
                            <a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
                                <span class="profile-text">Hello, Dharmesh</span>
                                <img class="img-xs rounded-circle" src="{{asset('image/d.png')}}" alt="Profile image">
                            </a>
                           
                            <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
                                <a class="dropdown-item p-0">
                                    <div class="d-flex border-bottom">
                                        <div class="py-3 px-4 d-flex align-items-center justify-content-center">
                                            <i class="mdi mdi-bookmark-plus-outline mr-0 text-gray"></i>
                                        </div>
                                        <div class="py-3 px-4 d-flex align-items-center justify-content-center border-left border-right">
                                            <i class="mdi mdi-account-outline mr-0 text-gray"></i>
                                        </div>
                                        <div class="py-3 px-4 d-flex align-items-center justify-content-center">
                                            <i class="mdi mdi-alarm-check mr-0 text-gray"></i>
                                        </div>
                                    </div>
                                </a>
                                  {{-- <a class="dropdown-item" href="{{ url('admin/logout') }}">
                                    Sign Out
                                  </a> --}}
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                    Logout
                                </a>
                                <form id="logout-form" action="{{ 'App\Admin' == Auth::getProvider()->getModel() ? route('admin.logout') : route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </div>
                        </li>
                    </ul>
                    <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
                        <span class="mdi mdi-menu"></span>
                    </button>
                </div>
            </nav>
            <!-- partial -->
            <div class="container-fluid page-body-wrapper">
                <!-- partial:partials/_sidebar.html -->
                <nav class="sidebar sidebar-offcanvas" id="sidebar">
                    <ul class="nav">
                        <li class="nav-item nav-profile">
                            <div class="nav-link">
                                <div class="user-wrapper">
                                    <div class="profile-image">
                                        <img src="{{asset('image/d.png')}}" alt="profile image">
                                    </div>
                                    <div class="text-wrapper">
                                        <p class="profile-name">Richard V.Welsh</p>
                                        <div>
                                            <small class="designation text-muted">Manager</small>
                                            <span class="status-indicator online"></span>
                                        </div>
                                    </div>
                                </div>
                                {{-- <button class="btn btn-success btn-block">New Project<i class="mdi mdi-plus"></i></button> --}}
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{url('admin/dashboard')}}">
                                <i class="menu-icon mdi mdi-television"></i>
                                <span class="menu-title">Dashboard</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
                                <i class="menu-icon mdi mdi-content-copy"></i>
                                <span class="menu-title">Main Menu</span>
                                <i class="menu-arrow"></i>
                            </a>
                            <div class="collapse" id="ui-basic">
                                <ul class="nav flex-column sub-menu">
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{route('post.index')}}">Post</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{route('category.index')}}">Category</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{route('tag.index')}}">Tags</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </nav>
                <!-- partial -->
                <div class="main-panel">
                    <div class="content-wrapper">
                        @yield('content')
                    </div>
                    <!-- content-wrapper ends -->
                    <!-- partial:partials/_footer.html -->
                    <footer class="footer">
                        <div class="container-fluid clearfix">
                            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018
                                <a href="http://www.bootstrapdash.com/" target="_blank">Dharmesh Sonagra</a>. All rights reserved.
                            </span>
                            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with
                                <i class="mdi mdi-heart text-danger"></i>
                            </span>
                        </div>
                    </footer>
                    <!-- partial -->
                </div>
              <!-- main-panel ends -->
            </div>
            <!-- page-body-wrapper ends -->
        </div>
        <!-- container-scroller -->
        <!-- plugins:js -->
        <!-- jQuery library -->
{{--         <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
 --}}
        <!-- Popper JS -->
{{--         <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
 --}}
        <!-- Latest compiled JavaScript -->
{{--         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
 --}}        <script src="{{asset('vendor/backend/js/vendor.bundle.base.js')}}"></script>
        <script src="{{asset('vendor/backend/js/vendor.bundle.addons.js')}}"></script>
        <!-- endinject -->
        <!-- Plugin js for this page-->
        <!-- End plugin js for this page-->
        <!-- inject:js -->
        <script src="{{asset('js/backend_js/off-canvas.js')}}"></script>
        <script src="{{asset('js/backend_js/misc.js')}}"></script>
        <!-- endinject -->
        <!-- Custom js for this page-->
        <script src="{{asset('js/backend_js/dashboard.js')}}"></script>
        <script src="{{asset('ckeditor/ckeditor.js')}}"></script>
        <!-- End custom js for this page-->
        <script>
           $(function (){
            CKEDITOR.replace('editor1');
                $('.textarea').wyshtml5();
           });
        </script>
        <script type="text/javascript">
            $(document).ready(function() {
            $('.tag').select2();
            });
        </script>
    </body>
</html>